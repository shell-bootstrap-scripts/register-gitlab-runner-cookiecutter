set -o errexit

NETWORK_SETTINGS_LOCATION={{ cookiecutter.NETWORK_SETTINGS_SCRIPT_LOCATION }}
FIX_ALL_GOTCHAS_SCRIPT_LOCATION={{ cookiecutter.FIX_ALL_GOTCHAS_SCRIPT_LOCATION }}
if test -z ${CI_SERVER_URL+ONLYIFUNDEFINED}; then
  CI_SERVER_URL={{ cookiecutter.CI_SERVER_URL }}
fi
if test -z ${CI_API_V4_URL+ONLYIFUNDEFINED}; then
  CI_API_V4_URL="$CI_SERVER_URL/api/v4"
fi
if test -z ${CI_PROJECT_ID+ONLYIFUNDEFINED}; then
  echo "You must set CI_PROJECT_ID."
  exit 1
fi
if test -z ${CI_PROJECT_NAMESPACE_ID+ONLYIFUNDEFINED}; then
  echo "CI_PROJECT_NAMESPACE_ID is unset. This is fine since it's not used yet anyway."
fi

# Check that timeout is available before we try to register.
echo "timeout --help"
if ! command -v timeout; then exit 1; fi
echo "wget --help"
if ! command -v wget && ! command -v curl; then exit 1; fi
echo "cat /etc/os-release"
if ! cat /etc/os-release; then exit 1; fi
echo "dpkg --print-architecture"
if dpkg --print-architecture; then
  ARCH_SHORT=$(dpkg --print-architecture)
else
  if [ $(arch) = "x86_64" ]; then
    # x86_64 is a name of specific 64-bit ISA. This instruction set was released in 1999 by AMD (Advanced Micro Devices). AMD later rebranded it to amd64.
    ARCH_SHORT=amd64
  else
    echo "Cannot determine architecture"
    exit 1
  fi
fi
if [ -z ${REGISTER_LOCKED+ABC} ]; then
 # When downloading this script and running it, we almost always want --locked=false.
 # For testing purposes, we can set LOCKED=true in the GitLab CI settings.
 REGISTER_LOCKED=false
 echo "By default, this gitlab-runner is not permanently tied to any particular repository. To change that, set REGISTER_LOCKED=true."
else
 echo "REGISTER_LOCKED=$REGISTER_LOCKED"
fi
if [ -z ${RUNNER_EXECUTOR+ABC} ]; then
 RUNNER_EXECUTOR=docker
 echo "By default, this script registers the gitlab-runner with the Docker executor. To change that, set RUNNER_EXECUTOR=shell."
fi
if [ "$RUNNER_EXECUTOR" = "shell" ] && [ -z ${RUNNER_SHELL+ABC} ] && ! bash --help; then
  RUNNER_SHELL=sh
fi
echo "RUNNER_EXECUTOR=$RUNNER_EXECUTOR"
if [ $RUNNER_EXECUTOR = "docker" ]; then
  if [ -z ${DOCKER_IMAGE+ABC} ]; then
    DOCKER_IMAGE="alpine:edge"
  fi
  if [ -z ${DOCKER_VOLUMES+ABC} ]; then
    if [ -z ${PATH_TO_MOUNT_INTO_RUNNER+ABC} ]; then
      if [ -z ${NAME_OF_MOUNTED_VOLUME_ON_HOST+ABC} ]; then
        NAME_OF_MOUNTED_VOLUME_ON_HOST='gitlab-runner-read-only-mounted-volume'
      fi
      mkdir -p /mnt/$NAME_OF_MOUNTED_VOLUME_ON_HOST
      PATH_TO_MOUNT_INTO_RUNNER=/mnt/$NAME_OF_MOUNTED_VOLUME_ON_HOST
      cp environment.sh /mnt/$NAME_OF_MOUNTED_VOLUME_ON_HOST
    fi
    if ! ls $PATH_TO_MOUNT_INTO_RUNNER; then echo "$PATH_TO_MOUNT_INTO_RUNNER not found!"; exit 1; fi
    if [ -z ${NAME_OF_MOUNTED_VOLUME_ON_GUEST+ABC} ]; then
      NAME_OF_MOUNTED_VOLUME_ON_GUEST='gitlab-runner-read-only-mounted-volume'
    fi
    DOCKER_VOLUMES="$PATH_TO_MOUNT_INTO_RUNNER:/$NAME_OF_MOUNTED_VOLUME_ON_GUEST:ro"
  fi
fi
#if [ -z ${CI_REPOSITORY_RUNNER_REGISTRATION_TOKEN+ABC} ]; then echo "This script cannot run if CI_REPOSITORY_RUNNER_REGISTRATION_TOKEN is undefined."; exit 1; fi
# https://docs.gitlab.com/ee/tutorials/automate_runner_creation
# https://about.gitlab.com/blog/2023/07/06/how-to-automate-creation-of-runners
# This API token must have at least Maintainer role, *in addition* to checking the box for create_runner scope.
if [ -z ${GITLAB_API_TOKEN+ONLYIFUNDEFINED} ]; then
  echo "If you do not set GITLAB_API_TOKEN, then we will have to use the legacy registration token, so we will not be able to properly set the tag list or runner description."
  exit 1
fi
if [ -z ${CI_SERVER_URL+ABC} ]; then echo "This script cannot run if CI_SERVER_URL is undefined."; exit 1; fi

if [ -z ${DOCKER_GPUS+ABC} ]; then
  lspci -nn -k | grep 'VGA\|3d\|2d' || echo "lspci failed."
  lshw -class display || echo "lshw failed."
  hwinfo --gfxcard || echo "hwinfo failed."
  # If you don't have a GPU, hwinfo --gfxcard will still turn up something, such as VMware.
  if nvidia-smi || (hwinfo --gfxcard --short | grep nVidia) || (lshw -class display | grep NVIDIA) || (lspci -nn -k | grep VGA | grep NVIDIA); then
    DOCKER_GPUS=all
  fi
fi

if [ -z ${NETWORK_SETTINGS_LOCATION+ABC} ] || [ -z ${FIX_ALL_GOTCHAS_SCRIPT_LOCATION+ABC} ]; then
 echo "This script is intended to be run with NETWORK_SETTINGS_LOCATION and FIX_ALL_GOTCHAS_SCRIPT_LOCATION set. However, we will gamely attempt to proceed without the setup scripts."
else
 # We cannot check the certificate before we have the certificate.
 if ! test -f environment.sh; then
  wget --no-proxy --no-check-certificate $NETWORK_SETTINGS_LOCATION --output-document environment.sh || curl --noproxy '*' --insecure $NETWORK_SETTINGS_LOCATION --output environment.sh
 fi
 cat environment.sh
 if ! test -f fix_all_gotchas.sh; then
  wget --no-proxy --no-check-certificate $FIX_ALL_GOTCHAS_SCRIPT_LOCATION || curl --noproxy '*' --insecure $FIX_ALL_GOTCHAS_SCRIPT_LOCATION --output fix_all_gotchas.sh
 fi
 . ./fix_all_gotchas.sh
fi

apt-get update || dnf upgrade-minimal --assumeyes || yum update-minimal -y || echo "Failed to upgrade base packages; will try to proceed anyway."
jq --version || apt-get install --assume-yes jq || dnf install --assumeyes jq || yum install -y jq
if [ "$RUNNER_EXECUTOR" = "shell" ]; then
  # With the shell executor, the host must have git else you can gitlab-runner run but all jobs will fail with git: command not found
  git --version || apt-get install --assume-yes git || dnf install --assumeyes git || yum install -y git
fi
GITLAB_RUNNER_NIGHTLY_BUILD_URL=https://s3.amazonaws.com/gitlab-runner-downloads/main/binaries/gitlab-runner-linux-$ARCH_SHORT
echo "GITLAB_RUNNER_NIGHTLY_BUILD_URL=$GITLAB_RUNNER_NIGHTLY_BUILD_URL"
wget $GITLAB_RUNNER_NIGHTLY_BUILD_URL --output-document gitlab-runner || curl $GITLAB_RUNNER_NIGHTLY_BUILD_URL --output gitlab-runner || python -c "import requests; open('gitlab-runner', 'wb').write(requests.get('$GITLAB_RUNNER_NIGHTLY_BUILD_URL').content)"
chmod u+x gitlab-runner

# set -o allexport
. /etc/os-release
# set +o allexport
echo "ID=$ID"
echo "VERSION_ID=$VERSION_ID"
echo "VERSION_CODENAME=$VERSION_CODENAME"
if [ -z ${ID+ABC} ]; then echo "ID is undefined, something is wrong."; exit 1; fi
if [ -z ${VERSION_ID+ABC} ]; then echo "VERSION_ID is undefined, something is wrong."; exit 1; fi
# CentOS /etc/os-release does not have VERSION_CODENAME
RUNNER_TAG_LIST="$RUNNER_EXECUTOR,$ID,$ID$VERSION_ID,relatime"
if [ -z ${VERSION_CODENAME+ABC} ]; then
  echo "VERSION_CODENAME is not defined in /etc/os-release."
  if ls /etc/lsb-release; then
    . /etc/lsb-release
    if [ -z ${DISTRIB_CODENAME+ABC} ]; then
      echo "DISTRIB_CODENAME is not defined in /etc/lsb-release."
    fi
    echo "DISTRIB_CODENAME=$DISTRIB_CODENAME"
    VERSION_CODENAME=$DISTRIB_CODENAME
  fi
fi
if ! [ -z ${VERSION_CODENAME+ABC} ]; then
  RUNNER_TAG_LIST="$RUNNER_TAG_LIST,$VERSION_CODENAME"
fi
if ! [ -z ${DOCKER_GPUS+ABC} ]; then
  RUNNER_TAG_LIST="$RUNNER_TAG_LIST,gpu"
else
  # Sometimes we want to be sure that a long-running job, such as a Kaniko build, will *not* tie up a GPU box.
  # I haven't found a better solution than making a tag for all the runners that lack a GPU.
  RUNNER_TAG_LIST="$RUNNER_TAG_LIST,nogpu"
fi
if [ $DOCKER_PRIVILEGED = true ]; then
  RUNNER_TAG_LIST="$RUNNER_TAG_LIST,privileged"
fi
if [ "$RUNNER_EXECUTOR" = "shell" ]; then
  RUNNER_TAG_LIST="$RUNNER_TAG_LIST,$(id -u -n)"
fi
if ! [ -z ${NAME_OF_MOUNTED_VOLUME_ON_HOST+ABC} ]; then
  if [ "$NAME_OF_MOUNTED_VOLUME_ON_HOST" != 'gitlab-runner-read-only-mounted-volume' ]; then
    RUNNER_TAG_LIST="$RUNNER_TAG_LIST,mounted-$NAME_OF_MOUNTED_VOLUME_ON_HOST"
  fi
fi
echo "RUNNER_TAG_LIST=$RUNNER_TAG_LIST"

if ! test -z ${https_proxy+ABC}; then
  RUNNER_ENV="https_proxy=$https_proxy"
  # But how do we put multiple variables in RUNNER_ENV the way we can with the --env option?
fi
# For a Docker executor, in addition to setting RUNNER_ENV or --env to be able to git clone,
# we'll want to set variables *inside* the Docker container.
# First we check if the environment.sh has been specifically provided (overridden).
# Then we try to pull it live.
# If that fails, use the old environment.sh that the GitLab-Runner saved.
if ! test -z ${https_proxy+ABC}; then
  RUNNER_PRE_BUILD_SCRIPT="ls environment.sh || wget --no-proxy --no-check-certificate $NETWORK_SETTINGS_LOCATION --output-document environment.sh --no-clobber || wget --proxy off $NETWORK_SETTINGS_LOCATION --output-document environment.sh --no-clobber || cp /$NAME_OF_MOUNTED_VOLUME_ON_GUEST/environment.sh ."
fi

GITLAB_RUNNER_DESCRIPTION=$(hostname)

# Make a special tag unique to this runner, so that we can run a test job on this runner after creating it.
TAG_UNIQUE_TO_THIS_RUNNER="$GITLAB_RUNNER_DESCRIPTION"
RUNNER_TAG_LIST="$RUNNER_TAG_LIST,$TAG_UNIQUE_TO_THIS_RUNNER"
echo "RUNNER_TAG_LIST=$RUNNER_TAG_LIST"

# https://docs.gitlab.com/ee/tutorials/automate_runner_creation
# https://about.gitlab.com/blog/2023/07/06/how-to-automate-creation-of-runners
echo "curl --silent --request POST --url '$CI_API_V4_URL/user/runners' --data 'runner_type=project_type' --data 'project_id=$CI_PROJECT_ID' --data 'description=$GITLAB_RUNNER_DESCRIPTION' --data 'tag_list=$RUNNER_TAG_LIST'"
RUNNER_JSON=$(curl --silent --request POST --url "$CI_API_V4_URL/user/runners" --data "runner_type=project_type" --data "project_id=$CI_PROJECT_ID" --data "description=$GITLAB_RUNNER_DESCRIPTION" --data "tag_list=$RUNNER_TAG_LIST" --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN")
RUNNER_TOKEN=$(echo "$RUNNER_JSON" | jq -r '.token')
RUNNER_ID=$(echo "$RUNNER_JSON" | jq -r '.id')
echo "$RUNNER_JSON" > runner_token_from_curl

RUNNER_TAG_LIST="$RUNNER_TAG_LIST,$RUNNER_ID"
echo "RUNNER_TAG_LIST=$RUNNER_TAG_LIST"

# https://docs.gitlab.com/ee/api/runners.html#update-runners-details
curl --request PUT --url "$CI_API_V4_URL/runners/$RUNNER_ID" --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" --form "tag_list=$RUNNER_TAG_LIST"

./gitlab-runner register --help
echo "REGISTER_LOCKED=$REGISTER_LOCKED"
echo "REGISTER_ACCESS_LEVEL=$REGISTER_ACCESS_LEVEL"
# Do we want to use --tls-ca-file or --tls-cert-file?
# later might want to use --env and/or --pre-build-script
# Variables set outside this script, such as RUNNER_EXECUTOR, are carried in,
# but RUNNER_TAG_LIST set inside this script needs to be explicitly given to gitlab-runner.
# RUNNER_TAG_LIST=$RUNNER_TAG_LIST RUNNER_EXECUTOR=$RUNNER_EXECUTOR RUNNER_SHELL=$RUNNER_SHELL RUNNER_PRE_BUILD_SCRIPT=$RUNNER_PRE_BUILD_SCRIPT DOCKER_IMAGE=$DOCKER_IMAGE DOCKER_VOLUMES=$DOCKER_VOLUMES DOCKER_GPUS=$DOCKER_GPUS REGISTER_LOCKED=$REGISTER_LOCKED ./gitlab-runner register --non-interactive --token $RUNNER_TOKEN --url $CI_SERVER_URL --env https_proxy=$https_proxy --env no_proxy=$no_proxy
# --token prevents using RUNNER_TAG_LIST and REGISTER_LOCKED here, they must be set in the earlier API call.
# https://docs.gitlab.com/ee/ci/runners/new_creation_workflow.html#changes-to-the-gitlab-runner-register-command-syntax
RUNNER_EXECUTOR=$RUNNER_EXECUTOR RUNNER_SHELL=$RUNNER_SHELL RUNNER_PRE_BUILD_SCRIPT=$RUNNER_PRE_BUILD_SCRIPT DOCKER_IMAGE=$DOCKER_IMAGE DOCKER_VOLUMES=$DOCKER_VOLUMES DOCKER_GPUS=$DOCKER_GPUS ./gitlab-runner register --non-interactive --token $RUNNER_TOKEN --url $CI_SERVER_URL --env https_proxy=$https_proxy --env no_proxy=$no_proxy

if [ $(id -u) -eq 0 ]; then
  CONFIG_TOML_PATH='/etc/gitlab-runner/config.toml'
else
  CONFIG_TOML_PATH="$HOME/.gitlab-runner/config.toml"
fi
# In case a runner fails to get properly unregistered, we want to display something like
# token = "__REDACTED__"
# since we don't know who might see these logs.
# Right now this just deletes the whole token line because that's easier to do.
cat $CONFIG_TOML_PATH | sed '/token = /d'

