NETWORK_SETTINGS_LOCATION={{ cookiecutter.NETWORK_SETTINGS_SCRIPT_LOCATION }}
FIX_ALL_GOTCHAS_SCRIPT_LOCATION={{ cookiecutter.FIX_ALL_GOTCHAS_SCRIPT_LOCATION }}

# Do not run this script except on a VM.
# First we just check to make sure some commands we'll need are already installed.
# curl, we will install (and configure).
echo "whoami"
whoami
if ! whoami; then exit 1; fi
echo "usermod --help"
if ! usermod --help; then exit 1; fi

if ! command -v wget && ! command -v curl; then
  apt-get install --assume-yes wget || dnf install --assumeyes wget || yum install -y wget
fi

if [ -z ${NETWORK_SETTINGS_LOCATION+ABC} ] || [ -z ${FIX_ALL_GOTCHAS_SCRIPT_LOCATION+ABC} ]; then
 echo "This script is intended to be run with NETWORK_SETTINGS_LOCATION and FIX_ALL_GOTCHAS_SCRIPT_LOCATION set. However, we will gamely attempt to proceed without the setup scripts."
else
 # We cannot check the certificate before we have the certificate.
 wget --no-proxy --no-check-certificate $NETWORK_SETTINGS_LOCATION --output-document environment.sh || curl --noproxy '*' --insecure $NETWORK_SETTINGS_LOCATION --output environment.sh
 cat environment.sh
 wget --no-proxy --no-check-certificate $FIX_ALL_GOTCHAS_SCRIPT_LOCATION || curl --noproxy '*' --insecure $FIX_ALL_GOTCHAS_SCRIPT_LOCATION --output fix_all_gotchas.sh
 . ./fix_all_gotchas.sh
fi

echo "install-docker: fix_all_gotchas.sh 1"
. ./fix_all_gotchas.sh

echo "install-docker: apt-get install curl"
# ubuntu:devel does not have sudo.
apt-get update || sudo apt-get update || dnf upgrade-minimal --assumeyes || sudo dnf upgrade-minimal --assumeyes || yum update-minimal -y || sudo yum update-minimal -y
apt-get install --assume-yes curl || sudo apt-get install --assume-yes curl || dnf install --assumeyes curl || sudo dnf install --assumeyes curl || curl --help || command -v curl || yum install -y curl || sudo yum install -y curl
if command -v apt; then
  # add-apt-repository is contained in software-properties-common.
  apt-get install --assume-yes software-properties-common || sudo apt-get install --assume-yes software-properties-common
fi
apt-get install --assume-yes ca-certificates || sudo apt-get install --assume-yes ca-certificates || dnf install --assumeyes ca-certificates || sudo dnf install --assumeyes ca-certificates || yum upgrade -y ca-certificates || sudo yum install -y ca-certificates
# apt-get install --assume-yes gnupg || sudo apt-get install --assume-yes gnupg
# if ! [ -z ${PROXY_CA_PEM+ABC} ]; then apt-key add "$PROXY_CA_PEM"; fi
# Now that curl is installed, re-run fix_all_gotchas.sh to configure curl.
echo "install-docker: fix_all_gotchas.sh 2"
. ./fix_all_gotchas.sh
if [ -z ${PROXY_CA_PEM+ABC} ]; then
  echo 'no PROXY_CA_PEM'
else
  if ls /etc/pki/ca-trust/source/anchors/; then
    cp $PROXY_CA_PEM /etc/pki/ca-trust/source/anchors/
  fi
fi
if command -v update-ca-trust; then
  update-ca-trust
fi
# get.docker.com will fail without dnf (dnf: command not found)
if command -v yum && ! command -v dnf; then
  yum install -y dnf
fi
# https://github.com/docker/docker-install/issues/49
# Working as intended. RHEL is not supported by Docker CE which is what this installation script installs.
# Maddeningly, /etc/redhat-release exists on systems that are not Red Hat.
if cat /etc/redhat-release && false; then
  echo "Docker does not support Red Hat, since Red Hat does not permit images to test the installation process. For the same reason, we cannot test installation on Red Hat, so we can only hope that what follows works."
  sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
  sudo yum install -y yum-utils device-mapper-persistent-data lvm2
  sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
  sudo yum install -y docker-ce docker-ce-cli containerd.io
else
  echo "install-docker: curl get.docker.com"
  curl --show-error --location https://raw.githubusercontent.com/dHannasch/docker-install/allow-rhel-to-use-centos/install.sh --output get-docker.sh
  # curl --show-error --location https://get.docker.com --output get-docker.sh
  echo "install-docker: sh get-docker.sh"
  sh get-docker.sh
fi

usermod --groups docker --append $(whoami)

if hwinfo --gfxcard --short | grep nVidia || (lshw -class display | grep NVIDIA) || (lspci -nn -k | grep VGA | grep NVIDIA) || ! [ -z $FORCE_ATTEMPT_INSTALL_NVIDIA_DRIVER ]; then
  # https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#setting-up-docker
  # https://github.com/NVIDIA/nvidia-docker/issues/1268
  # nvidia-docker2: This package is the only docker-specific package of any of them. It takes the script associated with the nvidia-container-runtime and installs it into docker's /etc/docker/daemon.json file for you. This then allows you to run (for example) docker run --runtime=nvidia ... to automatically add GPU support to your containers.
  # if you only install nvidia-container-toolkit (which is recommended for Docker 19.03+), then you will not get nvidia-container-runtime installed as part of it, and thus --runtime=nvidia will not be available to you. This is OK for Docker 19.03+ because it calls directly out to nvidia-container-toolkit when you pass it the --gpus option instead of relying on the nvidia-container-runtime as a proxy.
  echo "install-docker: found GPU, so attempting to install nvidia-container-toolkit."
  sh install-nvidia-driver.sh
  # --silent so curl doesn't dump garbage into apt-key add if it fails.
  distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
  if command -v apt; then
    distribution=$distribution curl --silent --location https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
    distribution=$distribution curl --silent --location https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
    apt-get update
    apt-get install --assume-yes nvidia-container-toolkit
  fi
  if command -v yum; then
    curl --silent --location https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.repo | sudo tee /etc/yum.repos.d/nvidia-docker.repo
    yum clean expire-cache
    yum install -y nvidia-container-toolkit
  fi
else
  echo "install-docker: did not find GPU, so skipping nvidia-container-toolkit."
fi

mkdir -p /etc/systemd/system/docker.service.d
echo "[Service]" > /etc/systemd/system/docker.service.d/http-proxy.conf
echo "Environment=\"HTTP_PROXY=$http_proxy\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf
echo "Environment=\"HTTPS_PROXY=$https_proxy\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf
echo "Environment=\"NO_PROXY=$no_proxy\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf
if command -v systemctl; then
  systemctl daemon-reload || echo "systemctl daemon-reload failed, hopefully just because this script is running inside a Docker container for testing."
  systemctl restart docker || echo "systemctl restart docker failed, hopefully just because this script is running inside a Docker container for testing."
  systemctl status docker || echo "systemctl status docker failed, hopefully just because this script is running inside a Docker container for testing."
  systemctl show --property=Environment docker || echo "systemctl show failed, hopefully just because this script is running inside a Docker container for testing."
fi

