NETWORK_SETTINGS_LOCATION={{ cookiecutter.NETWORK_SETTINGS_SCRIPT_LOCATION }}
FIX_ALL_GOTCHAS_SCRIPT_LOCATION={{ cookiecutter.FIX_ALL_GOTCHAS_SCRIPT_LOCATION }}

if ! command -v wget && ! command -v curl; then
  apt-get install --assume-yes wget || dnf install --assumeyes wget || yum install -y wget
fi

if [ -z ${NETWORK_SETTINGS_LOCATION+ABC} ] || [ -z ${FIX_ALL_GOTCHAS_SCRIPT_LOCATION+ABC} ]; then
 echo "This script is intended to be run with NETWORK_SETTINGS_LOCATION and FIX_ALL_GOTCHAS_SCRIPT_LOCATION set. However, we will gamely attempt to proceed without the setup scripts."
else
 # We cannot check the certificate before we have the certificate.
 wget --no-proxy --no-check-certificate $NETWORK_SETTINGS_LOCATION --output-document environment.sh || curl --noproxy '*' --insecure $NETWORK_SETTINGS_LOCATION --output environment.sh
 cat environment.sh
 wget --no-proxy --no-check-certificate $FIX_ALL_GOTCHAS_SCRIPT_LOCATION || curl --noproxy '*' --insecure $FIX_ALL_GOTCHAS_SCRIPT_LOCATION --output fix_all_gotchas.sh
 . ./fix_all_gotchas.sh
fi
# To add-apt-repository, we need to trust the certificate.
# fix_all_gotchas.sh will invoke update-ca-certificates if available, but will not install it.
# update-ca-certificates is in ca-certificates.
apt-get update || dnf update-minimal --assumeyes || yum update-minimal -y
apt-get install --assume-yes ca-certificates || sudo apt-get install --assume-yes ca-certificates || dnf install --assumeyes ca-certificates || sudo dnf install --assumeyes ca-certificates || yum upgrade -y ca-certificates || sudo yum install -y ca-certificates
. ./fix_all_gotchas.sh

apt-get install --assume-yes hwinfo lshw pciutils || dnf install --assumeyes lshw pciutils || yum install -y lshw pciutils
echo "FORCE_ATTEMPT_INSTALL_NVIDIA_DRIVER=$FORCE_ATTEMPT_INSTALL_NVIDIA_DRIVER"
echo "First, we just double- and triple-check that we can actually find the GPU."
lspci -nn -k | grep 'VGA\|3d\|2d' || echo "lspci failed."
lshw -class display || echo "lshw failed."
hwinfo --gfxcard || echo "hwinfo failed."
# If you don't have a GPU, hwinfo --gfxcard will still turn up something, such as VMware.
if (hwinfo --gfxcard --short | grep nVidia) || (lshw -class display | grep NVIDIA) || (lspci -nn -k | grep VGA | grep NVIDIA) || ! [ -z $FORCE_ATTEMPT_INSTALL_NVIDIA_DRIVER ]; then
  . /etc/os-release
  echo "VERSION_ID=$VERSION_ID"
  MAJOR_VERSION=$(echo $VERSION_ID | cut -c 1-1)
  if command -v dnf; then
    dnf install --assumeyes epel-release || dnf install --assumeyes https://dl.fedoraproject.org/pub/epel/epel-release-latest-${MAJOR_VERSION}.noarch.rpm
    dnf install --assumeyes kernel kernel-core kernel-modules
    echo "kernel-devel-$(uname -r) kernel-headers-$(uname -r)"
    # Error: Unknown repo: 'epel-next'
    dnf install --assumeyes kernel-devel kernel-headers dkms
  elif command -v yum; then
    yum install --assumeyes https://dl.fedoraproject.org/pub/epel/epel-release-latest-$(echo $VERSION_ID | tr '.' '-').noarch.rpm
    # yum install -y kernel-devel-$(uname -r) kernel-headers-$(uname -r)
    # No package kernel-devel-4.19.78-coreos available.
    # No package kernel-headers-4.19.78-coreos available.
    yum install -y kernel-devel kernel-headers
    yum install --assumeyes dkms
    yum update --assumeyes
    yum search kernel-devel-uname-r
    echo "kernel-devel-uname-r == $(uname -r)" "kernel-headers-uname-r == $(uname -r)"
    # yum install -y "kernel-devel-uname-r == $(uname -r)" "kernel-headers-uname-r == $(uname -r)"
  fi
  # In Ubuntu 18, the default channels have a new enough version of the nVidia driver,
  # but in Ubuntu 16, we need to add the PPA.
  # add-apt-repository is in software-properties-common.
  # yum-config-manager --add-repo is in yum-utils.
  apt-get update || dnf update-minimal --assumeyes || yum update-minimal -y
  echo "apt-get install --assume-yes software-properties-common"
  apt-get install --assume-yes software-properties-common || (dnf install --assumeyes 'dnf-command(config-manager)' && dnf config-manager --set-enabled PowerTools) || yum install -y yum-utils
  if command -v rpm; then
    distribution=$(. /etc/os-release;echo $ID`rpm -E "%{?rhel}%{?fedora}"`)
    echo "distribution=$distribution"
    distribution="rhel${MAJOR_VERSION}"
  fi
  ARCH=$(arch)
  echo "http_proxy=$http_proxy https_proxy=$https_proxy add-apt-repository --yes ppa:graphics-drivers"
  http_proxy=$http_proxy https_proxy=$https_proxy add-apt-repository --yes ppa:graphics-drivers || http_proxy=$http_proxy https_proxy=$https_proxy add-apt-repository --yes --massive-debug ppa:graphics-drivers || dnf config-manager --add-repo http://developer.download.nvidia.com/compute/cuda/repos/$distribution/${ARCH}/cuda-$distribution.repo || yum-config-manager --add-repo http://developer.download.nvidia.com/compute/cuda/repos/$distribution/${ARCH}/cuda-$distribution.repo
  echo "apt-get update"
  apt-get update || (dnf clean expire-cache && dnf update-minimal --assumeyes) || (yum clean expire-cache && yum update-minimal -y)
  # apt-get install --assume-yes nvidia-prime
  # prime-select query
  if command -v apt; then
    echo "apt-get install --assume-yes ubuntu-drivers-common"
    apt-get install --assume-yes ubuntu-drivers-common
    . /etc/os-release
    if [ $VERSION_ID = "18.04" ]; then
      export LC_ALL=C.UTF-8
      export LANG=C.UTF-8
    fi
    echo "ubuntu-drivers devices"
    ubuntu-drivers devices
    apt-cache search nvidia-driver
    echo "http_proxy=$http_proxy https_proxy=$https_proxy DEBIAN_FRONTEND=noninteractive ubuntu-drivers autoinstall"
    http_proxy=$http_proxy https_proxy=$https_proxy DEBIAN_FRONTEND=noninteractive ubuntu-drivers autoinstall
    ubuntu-drivers devices
  fi
  # https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html
  if command -v dnf; then
    dnf module install --assumeyes nvidia-driver:latest-dkms
  elif command -v yum; then
    echo "yum install --assumeyes nvidia-driver-latest-dkms"
    yum install --assumeyes nvidia-driver-latest-dkms
  fi
  if command -v nvidia-modprobe; then
    if nvidia-modprobe; then
      echo "nvidia-modprobe returned true"
    else
      echo "nvidia-modprobe returned false"
    fi
  fi
  nvidia-smi || echo "Until you reboot, nvidia-smi will probably say Failed to initialize NVML: Driver/library version mismatch. Unfortunately, inside a Docker container there may be no way to test this; nvidia-smi will not work until the host OS has the same driver version as the guest OS and *the host* has been rebooted. (On CentOS-based systems, the error message is NVIDIA-SMI has failed because it couldn't communicate with the NVIDIA driver. Make sure that the latest NVIDIA driver is installed and running. But the solution is the same: reboot the machine and then nvidia-smi should work.)"
fi
