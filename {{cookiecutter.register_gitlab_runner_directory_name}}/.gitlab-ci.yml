include:
- local: 'register_shell_executor.yaml'
- project: 'shell-bootstrap-scripts/register-gitlab-runner'
  file: 'register_gitlab_runner.yaml'
  ref: master


stages:
- environment
- nvidia
- run-jobs


trigger_register_gitlab_runner_pipeline:
  extends: .trigger_register_gitlab_runner_pipeline
  stage: environment
  rules:
  - if: $OPENSTACK_PASSWORD
    changes:
    - register_gitlab_runner_pipeline_to_trigger.yaml
    - register_gitlab_runner.yaml

.get_environment_file:
  tags:
  - docker
  stage: environment
  image: alpine:edge
  script:
  - wget http://www.google.com
  artifacts:
    paths:
    - environment.sh
    expire_in: 1 hour

.no_wget_curl_so_must_get_environment_from_previous_stage:
  # Ubuntu seems to have no means to load environment.sh for itself,
  # so we need to get it from another OS.
  needs:
  - job: get_environment_file
    artifacts: true

register_gitlab-runner_shell_executor:
  extends: .register_ephemeral_shell_executor_run_one_job
  stage: run-jobs
  rules:
  - if: $CI_REPOSITORY_RUNNER_REGISTRATION_TOKEN
    changes:
    - .gitlab-ci.yml
    - register_shell_executor.yaml
    - register_gitlab-runner.sh
    when: on_success
  - if: '$CI_PIPELINE_SOURCE == "web"'
    when: on_success

job_to_run_on_ephemeral_shell_executor:
  stage: run-jobs
  tags:
  - shell
  - ubuntu
  - relatime
  rules:
  - if: $CI_REPOSITORY_RUNNER_REGISTRATION_TOKEN
    changes:
    - .gitlab-ci.yml
    - register_shell_executor.yaml
    - register_gitlab-runner.sh
    when: on_success
  - if: '$CI_PIPELINE_SOURCE == "web"'
    when: on_success
  script:
  - echo success

register_gitlab-runner_docker_executor:
  stage: run-jobs
  tags:
  - docker
  variables:
    CONTAINER_IMAGE_PREFIX: $CI_REGISTRY/
    CONTAINER_IMAGE_NAMESPACE: shell-bootstrap-scripts
    CONTAINER_IMAGE_NAME: ubuntu-wget
    CONTAINER_IMAGE_TAG: latest
  image: ${CONTAINER_IMAGE_PREFIX}${CONTAINER_IMAGE_NAMESPACE}/${CONTAINER_IMAGE_NAME}:${CONTAINER_IMAGE_TAG}
  rules:
  - if: $CI_REPOSITORY_RUNNER_REGISTRATION_TOKEN
    changes:
    - .gitlab-ci.yml
    - install-docker.sh
    - register_gitlab-runner.sh
    when: on_success
  - if: '$CI_PIPELINE_SOURCE == "web"'
    when: on_success
  script:
  - . ./install-docker.sh
  - REGISTER_LOCKED=true REGISTER_ACCESS_LEVEL=ref_protected REGISTER_MAXIMUM_TIMEOUT=600 RUNNER_LIMIT=1 RUNNER_OUTPUT_LIMIT=1024 RUNNER_REQUEST_CONCURRENCY=1 sh ./register_gitlab-runner.sh || echo "The registration script failed in some way; just in case the gitlab-runner was left in a registered state, we're going to try to unregister."
  # - ./gitlab-runner list prints the secret Token
  # - ./gitlab-runner verify crashes with FATAL: no runner matches the filtering parameters and I'm not really sure what verify does in the first place
  # We want to make sure we always unregister, so we try to make sure this command always succeeds even if gitlab-runner crashes.
  - timeout --verbose --signal=SIGQUIT --kill-after=600 10 ./gitlab-runner run || echo "gitlab-runner run timed out *or* gitlab-runner failed to run."
  - ./gitlab-runner unregister --all-runners
  - cat /etc/gitlab-runner/config.toml || echo "No config.toml found, probably because the gitlab-runner never actually run, probably because this job ran inside an unprivileged Docker container itself."

register_gitlab-runner_docker_executor_centos:
  extends: register_gitlab-runner_docker_executor
  # image: dahanna/centos:centos-wget
  image: quay.io/centos/centos:stream

register_gitlab-runner_docker_executor_fedora:
  extends: register_gitlab-runner_docker_executor
  # image: fedora:21
  # image: fedora
  image: dahanna/fedora:wget-fedora

register_gitlab-runner_docker_executor_ubuntu16:
  extends: register_gitlab-runner_docker_executor
  image: dahanna/ubuntu:wget-ubuntu16

# Since the registration job is fail-safe, the registration job can succeed even if the registration does not actually succeed.
# That's necessary so that we don't accidentally leave orphaned gitlab-runner registrations lying around, but it means that we don't know whether the registration actually succeeded.
# It would be nice to have a test job.
# That's easy enough for the shell executor as shown about, but for the Docker executor we have the problem that Docker cannot actually run inside a Docker container.
# We can get to the point of successfully *picking up* a job, but that job will always fail.
job_to_run_on_ephemeral_docker_executor:
  allow_failure: true
  stage: run-jobs
  tags:
  - docker
  - relatime
  # tag just to ensure doesn't get picked up by the default runner that's running the registration jobs
  rules:
  - if: $CI_REPOSITORY_RUNNER_REGISTRATION_TOKEN
    changes:
    - .gitlab-ci.yml
    - install-docker.sh
    - register_gitlab-runner.sh
    when: on_success
  - if: '$CI_PIPELINE_SOURCE == "web"'
    when: on_success
  script:
  - echo success

install_nvidia_driver:
  stage: nvidia
  tags:
  - docker
  variables:
    CONTAINER_IMAGE_PREFIX: $CI_REGISTRY/
    CONTAINER_IMAGE_NAMESPACE: shell-bootstrap-scripts
    CONTAINER_IMAGE_NAME: ubuntu-wget
    CONTAINER_IMAGE_TAG: latest
  image: ${CONTAINER_IMAGE_PREFIX}${CONTAINER_IMAGE_NAMESPACE}/${CONTAINER_IMAGE_NAME}:${CONTAINER_IMAGE_TAG}
  rules:
  - if: $CI_REPOSITORY_RUNNER_REGISTRATION_TOKEN
    changes:
    - .gitlab-ci.yml
    - install-nvidia-driver.sh
    when: on_success
  - if: '$CI_PIPELINE_SOURCE == "web"'
    when: on_success
  script:
  - FORCE_ATTEMPT_INSTALL_NVIDIA_DRIVER=1 . ./install-nvidia-driver.sh

install_nvidia_driver_centos:
  extends: install_nvidia_driver
  image: quay.io/centos/centos:stream8
  # This will not work for stream9 yet because the repositories do not exist for 9 yet.

install_nvidia_driver_centos7:
  extends: install_nvidia_driver
  image: quay.io/centos/centos:7

.install_nvidia_driver_redhat:
  extends: install_nvidia_driver
  image: registry.access.redhat.com/ubi7/ubi

install_nvidia_driver_ubuntu18:
  extends: install_nvidia_driver
  variables:
    # These variables can be overridden by setting them on the project.
    # https://docs.gitlab.com/ee/ci/variables/#priority-of-cicd-variables
    DOCKER_BASE_IMAGE_PREFIX: $CI_REGISTRY/
    DOCKER_BASE_IMAGE_NAMESPACE: shell-bootstrap-scripts
    DOCKER_BASE_IMAGE_NAME: ubuntu-wget
    DOCKER_BASE_IMAGE_TAG: '18.04'
  image: ${DOCKER_BASE_IMAGE_PREFIX}${DOCKER_BASE_IMAGE_NAMESPACE}/${DOCKER_BASE_IMAGE_NAME}:${DOCKER_BASE_IMAGE_TAG}
