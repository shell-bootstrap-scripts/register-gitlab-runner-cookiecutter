1. `wget --no-proxy {{ cookiecutter.INSTALL_NVIDIA_DRIVER_SCRIPT_LOCATION }}`
2. `sudo sh install-nvidia-driver.sh`
1. `wget --no-proxy {{ cookiecutter.INSTALL_DOCKER_SCRIPT_LOCATION }}`
2. `sudo sh install-docker.sh`
2. `wget --no-proxy {{ cookiecutter.REGISTER_GITLAB_RUNNER_SCRIPT_LOCATION }}`
3. `sudo CI_REPOSITORY_RUNNER_REGISTRATION_TOKEN=something sh register_gitlab-runner.sh`

Never run a privileged Docker executor. If you must run a privileged Docker executor, you can with `sudo CI_REPOSITORY_RUNNER_REGISTRATION_TOKEN=something RUNNER_NAME=name_tying_to_single_project_and_make_separate_runner_for_separate_project REGISTER_LOCKED=true REGISTER_ACCESS_LEVEL=ref_protected DOCKER_PRIVILEGED=true sh register_gitlab-runner.sh`.

Alternatively, if you don't *have* privileges and thus cannot run Docker, you can run a shell executor with `CI_REPOSITORY_RUNNER_REGISTRATION_TOKEN=something RUNNER_EXECUTOR=shell sh register_gitlab-runner.sh`.

How many resources a GitLab-Runner consumes depends on what jobs it is running. You *can* run a Docker GitLab-Runner on a 5GB disk (with 4GB occupied by the operating system, so really only 1GB available) and 2GB of RAM if you stick to small Docker images and `docker system prune --all --volumes` regularly.

To stop a running GitLab-Runner, get its pid with `ps aux` or the like, then `sudo kill -SIGQUIT` that pid.

To unregister, `sudo ./gitlab-runner unregister --all-runners`.
